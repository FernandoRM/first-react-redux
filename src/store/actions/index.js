import { ADD_ARTICLE } from "./actionsTypes";

export function addArticle(payload) {
  return { type: ADD_ARTICLE, payload };
}
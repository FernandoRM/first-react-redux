import { ADD_ARTICLE } from "../actions/actionsTypes";


const initialState = {
  articles: []
};

function rootReducer(state = initialState, action) {
  if (action.type === ADD_ARTICLE) {
    // state.articles.push(action.payload); // We don't must modify the inmutable state object with this
    return Object.assign({}, state, {
      articles: state.articles.concat(action.payload)
    });
  }
  return state;
};

export default rootReducer;
import React from "react";
import { connect } from "react-redux";


const mapStateToProps = state => {
  return { articles: state.articles };
};

class ConnectedList extends React.Component {

  state = {
    articles: [
      { title: "First lines of React Redux", id: 1 },
      { title: "How to use React with Redux", id: 2 }
    ]
  }
  
  render() {

    const { articles } = this.state; 

    return (
      <ul className="list-group list-group-flush">
        {articles.map(el => (
          <li className="list-group-item" key={el.id}>
            {el.title}
          </li>
        ))}
      </ul>
    );
    
  }
}

const List = connect(mapStateToProps)(ConnectedList);

export default List;
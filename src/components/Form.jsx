import React, { Component } from "react";

class Form extends Component {
  constructor(props) {
    super(props);

    this.initialState = {
      name: "",
      job: ""
    };

    this.state = this.initialState;
  }

  handleChange = event => {
    const { name, value } = event.target;

    this.setState({
      [name]: value
    });
  };

  submitForm = () => {
    this.props.handleSubmit(this.state);
    this.setState(this.initialState);
  };

  render() {
    const { name, job } = this.state;

    return (
      <form className="form-container">
        <div className="form-group">
          <label>Name</label>
          <input
            type="text"
            name="name"
            value={name}
            onChange={this.handleChange}
          />
        </div>
        <div className="form-group">
          <label>Job</label>
          <input
            type="text"
            name="job"
            value={job}
            onChange={this.handleChange}
          />
        </div>
        <input className="button__submit" type="button" value="Submit" onClick={this.submitForm} />
      </form>
    );
  }
}

export default Form;

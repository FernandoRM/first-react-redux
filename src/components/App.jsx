import React from "react";
// import { connect } from "react-redux";

/* import store from "../store/index";
import { addArticle } from "../store/actions/index"; */

import List from "./List";
import Table from "./Table";
import Form from "./Form";

import logo from "../logo.svg";
import "./App.css";


/* window.store = store;
window.addArticle = addArticle;
store.subscribe(() => console.log('Look ma, Redux!!')); */

class HelloMessage extends React.Component { 
  render() {
    return (
      <h2 className="title--heading">Hello, {this.props.name}!&nbsp;&nbsp;
        <a
          className="link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
          >
           Learn React
        </a>
      </h2>
    );
  }
}

class App extends React.Component {
  /* const mapStateToProps = state => {
    return { articles: state.articles, characters: state.characters };
  }; */

  state = {
    characters: [
      {
        name: "Charlie",
        job: "Janitor"
      },
      {
        name: "Mac",
        job: "Bouncer"
      },
      {
        name: "Dee",
        job: "Aspring actress"
      },
      {
        name: "Dennis",
        job: "Bartender"
      }
    ]
  };

  removeCharacter = index => {
    const { characters } = this.state;

    this.setState({
      characters: characters.filter((character, i) => {
        return i !== index;
      })
    });
  };

  handleSubmit = character => {
    this.setState({ characters: [...this.state.characters, character] });
  };

  render() {
    const { characters } = this.state;

    return (
      <main className="container">
        <header className="header">
          <img src={logo} className="logo" alt="logo" />
          <HelloMessage name="Titi" />
        </header>
        <section className="container-body">
          <h3>Add a <code>character</code> with a name and a job to the table.</h3>
          <Table
            characterData={characters}
            removeCharacter={this.removeCharacter}
          />
          <h3>Add New</h3>
          <Form handleSubmit={this.handleSubmit} />
          <div className="row mt-5">
          <div className="col-md-4 offset-md-1">
          <h3>Articles</h3>
            <List />
          </div>
        </div>       
         </section>
      </main>
    );
  }
}


export default App; 
